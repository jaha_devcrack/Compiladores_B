/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package about_menu;
import gui.C_main_Form;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author yo
 */
public class C_menu_interact {   
    private JFileChooser JF_chooser;
    private String public_folder_path;
    private File master_file;
    
    public C_menu_interact() {              
        this.JF_chooser = new JFileChooser();        
        this.public_folder_path = "./public";
        this.JF_chooser.setCurrentDirectory(new File(this.public_folder_path));    
        this.master_file = new File (this.public_folder_path);
    }
    
    
    public File open_file_dialog(C_main_Form tmp_mn_form) {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Programas tiny", "tiny");
        
        this.JF_chooser.setApproveButtonText("Abrir");        
        this.master_file = null;        
        this.JF_chooser.setFileFilter(filter);        
        if (this.JF_chooser.showOpenDialog(tmp_mn_form) == JFileChooser.APPROVE_OPTION) 
            this.master_file = this.JF_chooser.getSelectedFile();              
        return this.master_file;
    }
    
    
    public File save_file_dialog(C_main_Form tmp_mn_form) {                
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Programas tiny", "tiny");
        
        this.master_file = null;
        this.JF_chooser.setApproveButtonText("Guardar");              
        this.JF_chooser.setFileFilter(filter);        
        if (this.JF_chooser.showOpenDialog(tmp_mn_form) == JFileChooser.APPROVE_OPTION) {
            this.master_file = this.JF_chooser.getSelectedFile();
            if (!this.master_file.getName().endsWith(".tiny")) 
                this.master_file = new File(this.master_file.getAbsolutePath() + ".tiny");            
        }        
        return this.master_file;        
    }
    
    private  boolean check_folder_exist(){
        this.master_file = new File(this.public_folder_path);        
        if(this.master_file.exists())
            return true;
        return false;
    }
    
    
    public void create_folder() {
        if(!this.check_folder_exist()) {
            this.master_file = new File(this.public_folder_path);
            try {
                this.master_file.mkdirs();              
            } 
            catch (SecurityException se) {               
                System.out.println(se.getMessage());
            }
        }
        
    }    
}
