/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package About_files_andStrings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import javax.swing.JTextArea;

/**
 *
 * @author devcrack
 */
public class C_manage_strings {
    JTextArea input_source;
    
    public C_manage_strings(JTextArea a_text_area) {
        this.input_source = a_text_area;
    }
    
    
    public ArrayList read_lines_buffer() {       
        String[] lines = this.input_source.getText().split("\\n");
        ArrayList<String> a_lines;
        
        a_lines = new ArrayList<>();                
        for(String line : lines) {
            if(!line.isEmpty())
                a_lines.add(line);
        }              
        return a_lines;
    }
    
    public ArrayList get_all_symbols(){
        ArrayList<String> lines = this.read_lines_buffer();
        ArrayList<String> all_symbols;
        
        all_symbols = new ArrayList<>();
        
        for (Iterator<String> it = lines.iterator(); it.hasNext();) {
            String a_string = it.next();
            String[]inner_symbols = a_string.split("\\s+");
            
            for(String str : inner_symbols) {
                if(!str.isEmpty())
                    all_symbols.add(str);
            }
        }
        return all_symbols;
    }
}
