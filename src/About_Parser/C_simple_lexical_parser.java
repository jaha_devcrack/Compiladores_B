/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package About_Parser;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author devcrack
 */
public class C_simple_lexical_parser {
    /**
     * 
     */
    Pattern pattern;
    /**
     * 
     */
    Matcher matcher;
    /**
     * Lista de simbolos totales sin analisar.
     */    
    private ArrayList<String> symbols;
    /**
     * Lista que almacena las palabras reservadas.
     */
    private ArrayList<String> key_words; 
    /**
     * Lista que almacena los separadores.
     */
    private ArrayList<String> separator_s;
    /**
     * Lista que almacena los operadores.
     */
    private ArrayList<String> operator_s;
    /**
     * Lista que almacena los numeros.
     */
    private ArrayList<String> number_s;
    /**
     * Lista que almacena los identificadores.
     */
    private ArrayList<String> id_s;
    /**
     * Lista que almacena los errores detectados.
     */
    private ArrayList<String> error_s;
    
    
    /**
     * Definicion de las palabras reservadas.
     */
    static final String[] KEY_WORDS = {"INT", "REAL", "STRING", "MAIN", "BEGIN",
        "IF", "ELSE", "RETURN","END","READ", "WRITE"};    
    /**
     * Definicion de Caracteres especiales, Separadores.
     */
    static final String[] SEPARATORS = {";", "," , "\\(", "\\)"};
    /**
     * Definicion de Operadores.
     */
    static final String[] OPERATORS = {"\\+", "\\-", "\\*", "/", "\\:\\="};            
    
    /**
     * Definicion de numeros.
     */
    static final String NUMBERS = "\\d+";
    /**
     * Definicion de expresion regular para Identicadores.
     */
     static final String IDS = "[_a-zA-Z][_a-zA-Z0-9]*";  
    
    /***
     * Inicializa la clase C_simple_lexical_parser.
     * @param a_symbols Lista de de cadenas sin analisar, solamente no tienen espacios entre si.
     */
    public C_simple_lexical_parser(ArrayList<String> a_symbols) { 
        this.symbols = a_symbols; 
        this.key_words = new ArrayList<String>();
        this.separator_s = new ArrayList<String>();
        this.operator_s =  new ArrayList<String>();        
        this.id_s = new ArrayList<String>();
        this.number_s = new ArrayList<String>();
        this.error_s = new ArrayList<String>();
    }
    
    /**
     * Incializa todas las variables globales de la clase.
     */
    public C_simple_lexical_parser() { 
        this.symbols = new ArrayList<String>();
        this.key_words = new ArrayList<String>();
        this.operator_s =  new ArrayList<String>();        
        this.id_s = new ArrayList<String>();
    }

    
    /**
     * Se empieza el analisis de cada simbolo lexicamente correcto en el lenguaje tiny.
     */
    public void fill_list_with_dummy_parser() {
        String current_state = null;
        
        for(String char_s : this.symbols) {
            current_state = this.match_KEY_WORDS(char_s);
            if(current_state != null && !current_state.isEmpty()) { //Esta cadena aun se puede seguir analisando.
                System.out.println("\n\nStarting with SEPARATORS lexer parser");  
                current_state = this.match_separators(current_state);
                if(current_state != null && !current_state.isEmpty()) {
                    System.out.println("\n\nStarting with OPERATORS lexer parser");  
                    current_state = this.match_operators(current_state);
                    if(current_state != null && !current_state.isEmpty()) {
                        System.out.println("\n\nStarting with numbers lexer parser");
                        current_state = this.match_numbers(current_state); 
                        if(current_state != null) {
                            System.out.println("\n\nStarting with ID'S lexer parser");
                            current_state = this.match_id_s(current_state);  
                            if(current_state != null)
                                this.error_s.add(current_state);
                        }
                    }
                }
            }
        }                
    }    
    
       
    /**
     * Determina si una o mas palabras reservadas del lenguaje TINY coinciden dentro de la cadena a analisar.
     * @param a_str cadena que se esta analizando
     * @return Regresa el resto de la cadena para continuar con el analisis o null si ya no hay nada mas que analisar.
     */
    private String match_KEY_WORDS(String a_str) {       
        String str_updated = a_str;
        int a_start = 0;
        int a_end = 0;
        int str_length = a_str.length();
        
        System.out.flush();
        System.out.println("Current string: " + a_str);
        for(String kw_regex : KEY_WORDS) {            
            this.pattern = Pattern.compile(kw_regex);                           //Se establece nuestra expresion regular que basicamente es la palabra reservada
            this.matcher = pattern.matcher(a_str);                        
            if(matcher.matches()) {                                             //Verificamos si de principio a fin se completa la verificacion de coincidencia.
                System.out.println("KEYWORD added\n");  
                this.getKey_words().add(kw_regex);                                   // Agregamos a nuestra lista de tokens de de palabras reservadas.
                return null;                                                    //Retornamos null, dado que no hay nada mas que analisar.
            }
            else {                                   
                String valid_case = " ";        
                
                while (matcher.find()) {                                        //Verificamos si hay coincidencia de nuestro patron dentro de la cadena. 
                    a_start = matcher.start();
                    a_end = matcher.end();
                    if(a_start != 0 && a_end != str_length) { //Puede ser que la palabra reservada se encuentre en medio de la cadena.
                        valid_case = ".*\\("+kw_regex + "\\).*";                     //Redefinimos la expresion regular para el unico caso valido.
                        //Reconstruimos el regex a partir de la nueva expresion regular.
                        this.pattern = Pattern.compile(valid_case);
                        this.matcher = pattern.matcher(a_str);
                        if(matcher.matches()) {                            
                            this.getKey_words().add(kw_regex);                       // Agregamos a nuestra lista de tokens de de palabras reservadas.           
                            str_updated = a_str.replace(kw_regex, "");          // Eliminamos de la cadena analisada el patron encontrado
                            System.out.println("New string to analize : " + str_updated);  
                            System.out.println("KEYWORD found betwen chars, we added\n");  
                            return str_updated;
                        }
                    }
                    else if(a_start != 0) {                                     //La palabra reservada se encuentra al final de la cadena.?
                        valid_case = ".*\\("+kw_regex;                          //Redefinimos la expresion regular para el unico caso valido.
                        this.pattern = Pattern.compile(valid_case);
                        this.matcher = pattern.matcher(a_str);
                        if(matcher.matches()) {                              
                            this.getKey_words().add(kw_regex);                                
                            str_updated = a_str.replace(kw_regex, "");            
                            System.out.println("New string to analize : " + str_updated);  
                            System.out.println("KEYWORD found at end of string, we added\n");
                            return str_updated;
                        }                          
                    }
                    else {                                                      //La palabra reservada se encuentra al principio de la cadena?
                        if(kw_regex =="READ" | kw_regex == "WRITE")
                            valid_case = kw_regex + "\\(.*";
                        else
                            valid_case = kw_regex + "\\).*";                    //Redefinimos la expresion regular para el unico caso valido.
                        this.pattern = Pattern.compile(valid_case);
                        this.matcher = pattern.matcher(a_str);
                        if(matcher.matches()){                            
                            this.getKey_words().add(kw_regex);
                            str_updated = a_str.replace(kw_regex, "");
                            System.out.println("New string to analize : " + str_updated);  
                            System.out.println("KEYWORD found at begin of string, we added\n");  
                            return str_updated;
                        }                        
                    }
                    System.out.println("Fkin weird word or I'd found , we added");  
                    this.getId_s().add(a_str);
                    return null;
                }
            }                
        }        
        return str_updated;
    }    

    /**
     * 
     * @param a_str Cadena a analisar.
     * @return Null si no hay nada mas que analisar en esta cadena, de lo contrario regresa una cadena con lo que resta por analisar.
     */
    private String match_separators(String a_str) {        
        String str_updated = a_str;
        int number_matches = 0;
        // En este caso falla:
       //z,file
    
        System.out.flush();
        System.out.println("Current string: " + a_str);
        for(String separator_regex : SEPARATORS) {                              //Analisamos cada uno de los simbolos separadores existentes.
            String aux_str = separator_regex;
            
            if(separator_regex == "\\(" | separator_regex == "\\)" )
                aux_str = separator_regex.substring(1);            
            number_matches = 0;
            this.pattern = Pattern.compile(separator_regex);                           //Se establece nuestra expresion regular que basicamente es el simbolo
            this.matcher = pattern.matcher(a_str);   
            if(matcher.matches()) {                                             //Verificamos si de principio a fin se completa la verificacion de coincidencia.
                System.out.println("Separator added\n");  
                this.separator_s.add(aux_str);                                   // Agregamos a nuestra lista de tokens de de palabras reservadas.
                return null;                                                    //Retornamos null, dado que no hay nada mas que analisar.
            }
            else {                                                              //Existe de verdad el simbolo?
                while(matcher.find()) {
                    number_matches++; 
                }
                if(number_matches > 0) {                    
                    System.out.println("SEPARATOR = "+ separator_regex);
                    System.out.println("Number Matches=" + number_matches);                    
                    for(int i = 0 ; i < number_matches; i++) {                                                
                        this.separator_s.add(aux_str);
                        str_updated = str_updated.replace(aux_str, "");                        
                    }                       
                }
            }
        }        
        return str_updated;
    }
   
    /**
     * Analisa la cadena para determinar si esta tiene operadores validos para lenguaje tiny.
     * @param a_str Cadena a analisar.
     * @return Null si no hay mas que analisar.
     */
    public String match_operators(String a_str) {
        String str_updated = a_str;
        int number_matches = 0;
        
        System.out.flush();
        System.out.println("Current string: " + a_str);
        for(String operator_regex : OPERATORS) {                              //Analisamos cada uno de los simbolos separadores existentes.
            String aux_str = operator_regex;
            if(operator_regex != "/" ) {
                if(operator_regex.compareTo("\\:\\=") == 0)
                    aux_str = operator_regex.replace("\\", "");
                else
                    aux_str = operator_regex.substring(1);
            }
            number_matches = 0;
            this.pattern = Pattern.compile(operator_regex);                           //Se establece nuestra expresion regular que basicamente es el simbolo
            this.matcher = pattern.matcher(a_str);   
            if(matcher.matches()) {                                             //Verificamos si de principio a fin se completa la verificacion de coincidencia.
                System.out.println("Operator added\n");  
                this.operator_s.add(aux_str);                                   // Agregamos a nuestra lista de tokens de de palabras reservadas.
                return null;                                                    //Retornamos null, dado que no hay nada mas que analisar.
            }
            else {
                while(matcher.find()) {
                    number_matches++; 
                }
                if(number_matches > 0 ) {
                    System.out.println("SEPARATOR = "+ operator_regex);
                    System.out.println("Number Matches=" + number_matches);  
                    for(int i = 0 ; i < number_matches; i++) {                                                
                        this.operator_s.add(aux_str);
                        str_updated = str_updated.replace(aux_str, "");                        
                    }   
                }
            }
        }        
        return str_updated;
    }
    
    /**
     * Detecta si la cadena que se esta analisando es un numero.
     * @param a_str Cadena que se esta analisando.
     * @return 
     */
    private String match_numbers(String a_str) {
        String str_update = a_str;
        
        System.out.flush();
        System.out.println("Current string: " + a_str);
        this.pattern = Pattern.compile(NUMBERS);                           //Se establece nuestra expresion regular que basicamente es el simbolo
        this.matcher = pattern.matcher(a_str);  
        if(matcher.matches()) {
            this.number_s.add(a_str);
            return null;
        }
        return str_update;
    }
    
    
    /**
     * Detecta si la cadena que se esta analisando es un identificador valido.
     * @param a_str Cadena que se esta analisando.
     * @return Null si no hay nada mas que analisar.
     */
    private String match_id_s(String a_str) {
        String str_update = a_str;
        
        System.out.flush();
        System.out.println("Current string: " + a_str);
        this.pattern = Pattern.compile(IDS);                           //Se establece nuestra expresion regular que basicamente es el simbolo
        this.matcher = pattern.matcher(a_str);  
        if(matcher.matches()) {
            this.id_s.add(a_str);
            return null;
        }
        return str_update;
    }
    
    
    /**
     * 
     * @param args 
     */ 
    public static void main(String args[]) {
        C_simple_lexical_parser test_dummy_parser = new C_simple_lexical_parser();
        
       test_dummy_parser.fill_list_with_dummy_parser();
    }

    /**
     * @return Regresa la lista de palabras reservadas.
     */
    public ArrayList<String> getKey_words() {
        return key_words;
    }

    /**
     * @return Retorna la lista de separadores.
     */
    public ArrayList<String> getSeparator_s() {
        return separator_s;
    }

    /**
     * @return the operator_s
     */
    public ArrayList<String> getOperator_s() {
        return operator_s;
    }

    /**
     * @return the number_s
     */
    public ArrayList<String> getNumber_s() {
        return number_s;
    }

    /**
     * @return the id_s
     */
    public ArrayList<String> getId_s() {
        return id_s;
    }

    /**
     * @return the error_s
     */
    public ArrayList<String> getError_s() {
        return error_s;
    }
}

/*    
Keywords:   WRITE READ IF ELSE RETURN BEGIN END MAIN STRING INT REAL
Single-character separators:   ;  ,  (   )
Single-character operators:    +  -  *   /
Multi-character operators:    :=  ==   !=
Identifier: An identifier consists of a letter followed by any number of letters or digits. The following are examples of identifiers: x, x2, xx2, x2x, End, END2.Note that End is an identifier while END is a keyword. The following are not identifiers:
*/
