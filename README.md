# Requerimientos
- Java 8
- Netbeans 8.2
  
  Se puede bajar desde aqui [Netbeans 8.2 con Java 8](http://www.oracle.com/technetwork/es/java/javase/downloads/jdk-netbeans-jsp-3413139-esa.html)
- Git 

 Podemos descargar Git para nuestro fabuloso windows desde aqui [Git para windows](https://github.com/git-for-windows/git/releases/download/v2.18.0.windows.1/Git-2.18.0-64-bit.exe). No es necesario instalar git ya que podemos descargar el proyecto y prescindir de este software.

# Como usar
Una vez que se han descargado e instalado los requerimientos lo primero que se tiene que hacer es descargar el proyecto. Para esto ingresar el comando: ```git clone https://gitlab.com/jaha_devcrack/Compiladores_B.git ``` o bien se puede descargar comprimido en .zip desde aqui [Proyecto Tiny0](https://gitlab.com/jaha_devcrack/Compiladores_B/-/archive/master/Compiladores_B-master.zip).  Solo queda abrir el proyecto con Netbeans y ejecutarlo.

###  Problemas (Issues)
Falto validar las cadenas como token valido dentro del lexico.

